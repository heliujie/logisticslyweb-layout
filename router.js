
import Home  from './home.vue'
import Mhome  from './m_home.vue'
import Login  from './../login/login.vue'
import Forgotpwd from './../forgotpwd/forgotpwd'
//import AppRegister from '@/components/common/app-register.vue'
import Register from './../register/register.vue'

export default [
    //空地址
    {
        path:'/',
        redirect:'/login'
    },
    //首页
    {
        path: '/home',
        component: Home
    },
    {
        path: '/mhome',
        name: 'mhome',
        component: Mhome
       /* children: [
            // 操作中心
            {
                name: 'operation-center',
                path: 'operation-center',
                component: (resolve) => require(['./operation-center'], resolve),
                meta: {
                    tabsField: 'opera_alivetablist',
                    resourceid: 10006
                }
            },
            // 结算中心
            {
                name: 'sett-center',
                path: 'sett-center',
                component: (resolve) => require(['./sett-center'], resolve),
                meta: {
                    tabsField: 'sett_alivetablist',
                    resourceid: 10008
                }
            }            
        ]*/
    },
    //登陆
    {
        path: '/login',
        component: Login
    },
    {
        path: '/forgotpwd',
        component: Forgotpwd
    },
    {
        path: '/register',
        component: Register
    }
]
