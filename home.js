import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './router'
import store from '@/store'
import axios from 'axios'

import interceptorsService from '@/services/interceptors_service'
interceptorsService(axios);
import Vuebar from 'vuebar';
import iview from 'iview'
import 'iview/dist/styles/iview.css'

Vue.prototype.$http = axios;
Vue.config.productionTip = false
Vue.use(VueRouter);
Vue.use(iview)
Vue.use(Vuebar)
const router = new VueRouter({
	routes
})

new Vue({
	router,
    store
}).$mount('#app')
