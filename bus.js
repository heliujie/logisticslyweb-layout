import Vue from 'vue'
import { officialWebsite }  from '@services/bussiness_service'
export default new Vue({
	data(){
		return {
			// 是否在更改主模块
			mainmoduleChangeFlag: false,
			// 默认菜单未收缩
        	menuCollepseStatus: false,			
			// 平级菜单资源
			ordinaryResourcelists: [],
			// 树级菜单资源
			ordinaryTreeResourcelists: [],
			// 主模块活跃索引
			currentMainSlideIndex: 0,
			// 操作中心模块下的选项卡
			opera_alivetablist: [],
			// 结算中心模块下的选项卡
			sett_alivetablist: [],
			// 当前选项卡索引
			currentTabIndex: 0,
			// 菜单分组依据数据
			divideGroupList: [],
			// 打开新选项卡预配置
			customMadeNewTabLists: [
				// 指派司机数据详情页
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888890,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888890					
				},
				// 新增承运商选项卡
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888888,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888888					
				},				
				// 新增发货人选项卡
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888889,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888889					
				},
				// 新增注册选项卡
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888891,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888891					
				},
				// 新增司机提货选项卡
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888892,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888892					
				},
				// 新增司机提货选项卡
				{
					// 是否激活选项卡
					alivestatus: false,
					// 是否刷新
					refreshflag: false,
					// 控制在哪个模块下 
					o_posresourceid: 1460,
					// 区分菜单与弹出选项卡
					tabtype: 'custommade',
					o_resourceid: 88888893,
			        o_resourcename: '',
			        o_weburl: '',
			        o_osresourceid: 88888893					
				}												
			],
			// 平级 => 树级 转换配置
			treeSettingOption: {
	                data: {
	                    key: {
	                        nodes: "nodes"
	                    },
	                    simpleData: {
	                        idKey: "o_osresourceid",
	                        pIdKey: "o_posresourceid",
	                        rootPId: 0
	                    }
	                }
		    }	
		}
	}
})